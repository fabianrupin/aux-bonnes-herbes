# Coding Style

.DEFAULT_GOAL := help

help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
##
## Project setup
##---------------------------------------------------------------------------
cs: ## check cs problem
	./vendor/bin/php-cs-fixer fix --dry-run --stop-on-violation --diff

cs-fix: ## fix problems
	tools/php-cs-fixer/vendor/bin/php-cs-fixer fix

cs-ci:
	./vendor/bin/php-cs-fixer fix src/ --dry-run --using-cache=no --verbose

db-migrate: ## make migrations and add fixtures
	symfony console doctrine:migrations:migrate -n

fix: ## fix all
	tools/php-cs-fixer/vendor/bin/php-cs-fixer fix
	./vendor/bin/phpstan
	./bin/console lint:twig templates
	./bin/console lint:yaml config --parse-tags
	./bin/phpunit



##
## start and stop server
##---------------------------------------------------------------------------

docker: ## start docker with database
	docker-compose up -d

start: ## start symfony server and watch dev
	symfony run -d yarn watch
	symfony serve --no-tls -d
	symfony open:local

db-create:
	symfony console doctrine:database:drop --env=dev --force --if-exists
	symfony console doctrine:database:create --env=dev
	symfony console doctrine:migrations:migrate -n
	symfony console doctrine:fixtures:load -n --env=dev

stop: ## stop server
	symfony server:stop
	docker-compose down

start-preprod: ## start symfony server and gulp dev
	yarn build
	symfony serve --no-tls -d

deploy:
	git pull
	composer install --no-dev --optimize-autoloader
	yarn install
	yarn build
	symfony console c:c

