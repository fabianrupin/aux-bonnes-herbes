<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MettriePlaceTest extends WebTestCase
{
    public function testMettriePlaceTest(): void
    {
        $client = static::createClient();
        $client->request('GET', '/mettrie/place');

        $this->assertResponseIsSuccessful();
    }
}
