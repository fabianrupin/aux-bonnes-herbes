<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BiographyTest extends WebTestCase
{
    public function testBiographyShow(): void
    {
        $client = static::createClient();
        $client->request('GET', '/biographie');

        $this->assertResponseIsSuccessful();
    }
}
