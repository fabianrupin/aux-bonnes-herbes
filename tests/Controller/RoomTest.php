<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RoomTest extends WebTestCase
{
    public function testSomething(): void
    {
        $client = static::createClient();
        $client->request('GET', '/location/le-gite');

        $this->assertResponseIsSuccessful();

        $client->request('GET', '/chambre/papillons');

        $this->assertResponseIsSuccessful();

        $client->request('GET', '/chambre/soleil-levant');

        $this->assertResponseIsSuccessful();
    }
}
