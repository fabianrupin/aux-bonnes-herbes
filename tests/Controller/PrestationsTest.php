<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PrestationsTest extends WebTestCase
{
    public function testNearbyShow(): void
    {
        $client = static::createClient();
        $client->request('GET', '/prestations');

        $this->assertResponseIsSuccessful();
    }
}
