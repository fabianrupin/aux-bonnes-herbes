<?php

namespace App\Service\Sentry;

class SentryFactory
{
    public static function createBeforeSendCallable(Sentry $sentry): callable
    {
        return [$sentry, 'beforeSend'];
    }
}
