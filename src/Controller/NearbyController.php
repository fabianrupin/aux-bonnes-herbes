<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NearbyController extends AbstractController
{
    #[Route(path: '/aux-alentours', name: 'nearby')]
    public function show(): Response
    {
        return $this->render('nearby/index.html.twig', [
            'controller_name' => 'NearbyController',
        ]);
    }
}
