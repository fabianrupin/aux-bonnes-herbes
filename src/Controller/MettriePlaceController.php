<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MettriePlaceController extends AbstractController
{
    #[Route(path: '/mettrie/place', name: 'mettrie_place')]
    public function show(): Response
    {
        return $this->render('mettrie_place/index.html.twig', [
            'controller_name' => 'MettriePlaceController',
        ]);
    }
}
