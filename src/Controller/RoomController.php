<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RoomController extends AbstractController
{
    #[Route(path: '/location/le-gite', name: 'le-gite-show')]
    public function leGiteShow(): Response
    {
        return $this->render('room/le_gite.html.twig', [
        ]);
    }

    #[Route(path: '/chambre/papillons', name: 'papillons-show')]
    public function papillonsShow(): Response
    {
        return $this->render('room/papillons.html.twig', [
        ]);
    }

    #[Route(path: '/chambre/soleil-levant', name: 'soleil-levant-show')]
    public function soleilLevantShow(): Response
    {
        return $this->render('room/soleil_levant.html.twig', [
        ]);
    }
}
